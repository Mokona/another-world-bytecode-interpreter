Contributing to this Project
============================

Every kind of help is welcome!

As a starter, you probably should read
[the project wiki.](https://framagit.org/ylebars/another-world-bytecode-interpreter/-/wikis/ "Project wiki")

To help you set up your development environment, there are some scripts
generating the recommended compiling environment in the _utils/_ directory.

Do not hesitate to contact Yoann Le Bars (you can find his coordinates in
[_AUTHORS.txt_](AUTHORS.txt "AUTHORS.txt")).

**Contents**

- [Process](#process "Process")
- [Coding conventions](#coding-conventions "Coding conventions")

Process
-------

Anyone can submit some change. You can submit patches as well as merging
requests. We prefer you submit several small commits rather than huge ones:
conflicts are more easily solved with small submissions.

Coding conventions
------------------

* Indentation should use four spaces and no tabulation;
* you should remove white-spaces at the end of lines;
* logically similar items should be aligned;
* curly braces should be on the same line as the logical statement that requires
them;
* on `if` statements, you should always include braces;
* the `\date` Doxygen field should indicate the date of the first and the last
edits of the file;
* you should avoid any loop in headers;
* to avoid troubles with **Microsoft Visual Studio**®, you should not do
implicit conversion;
* if the implementation of a method, a procedure, or a function need a
dependence (`#include`) unnecessary to the prototype, then the implementation
should not be in the header;
* naming conventions:
    1. names for `class`, `struct`, and `namespace` should start in upper-case,
    while names for objects, variables and function should start in lower-case;
    2. in names composed with several words, each word should be separate with
    upper-case;
    3. in general, names do not contain underscore;
    4. if a field has one or several accessors, the field and the accessors
    have the same name, but the name of the field is postfixed with an
    underscore;
    5. a parameter initialising a field has the same name as the field, but its
    name is prefixed with an underscore, while the name of the field follows
    the previous rules;
* comments guidelines:
    * comments should comply to Doxygen format;
    * for every class, structure, method, function, procedure, and name-space,
    the field `\brief` should be filled in;
    * for every method, function, and procedure, a `\param` field should be
    set for every parameters;
    * for every method and function, the field `\returns` should be filled in;
    * every declared variable should be documented with a Doxygen comment;
    * Doxygen documentation of classes, methods, functions, and procedures
    should be in headers files;
    * Doxygen documentation of name-spaces should be in the top header file;
    * the field `\date` should not be used, as Git does keep a precise track
    of modifications and when they happened.

To help you format your code, there is a _.clang-format_ file in the _src/_
directory. Do not hesitate to use CLang-format.
