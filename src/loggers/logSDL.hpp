#ifndef __LOGSDL_HPP__
#define __LOGSDL_HPP__

/**
 * \file logSDL.hpp
 * \brief class to implement the logs through SDL_Log
 * \author Willll
 * \version 0.1
 */

#include "../ilog.hpp"

#include <cstdarg>

#include <SDL_log.h>

namespace AnotherWorld {
    /// \brief Class for legacy logging.
    class LogSDL : public ILogger {
    public :
        /// \brief Default constructor disabled.
        LogSDL () = delete;

        /**
         * \brief Constructor.
         * \param isSet Whether or not logging.
         */
        explicit LogSDL (bool isSet):
                ILogger(isSet), severityLevel(SeverityLevels::DISABLE) {}

        /// \brief Destructor.
        virtual ~LogSDL () {}

        /**
         * \brief Sending a log message.
         * \param slvl Message severity level.
         * \param format Message format describer.
         */
        void log (SeverityLevels slvl, const char* format, ...) override;

        /**
         * \brief Sending a debug message.
         * \param cm Code for message to generate.
         * \param format Message format describer.
         */
        void debug (ILogger::DebugSource cm, const char* format,
                    ...) override;

        /**
        * \brief Sending a warning message.
        * \param format Message format describer.
        */
        void information (const char* format, ...) override;

        /**
         * \brief Sending a warning message.
         * \param format Message format describer.
         */
        void warning (const char* format, ...) override;

        /**
         * \brief Sending an error message.
         * \param format Message format describer.
         */
        void error (const char* format, ...) override;

        /**
         * \brief Setup logger
         * \param configuration Logger configuration object.
         */
        void setup (const LoggerConfiguration &configuration) override;

    protected:
        /// \brief Logs severity level.
        SeverityLevels severityLevel;

        /**
         * \brief Converts application log levels to SDL log level.
         * \param application Log level.
         * \returns SDL log level.
         */
        inline SDL_LogPriority toSDLLogLevel (const SeverityLevels &severityLevel) const;

        /**
        * \brief Base for every log message.
        * \param slvl Security level for logging.
        * \param format Message format describer.
        * \param va Command line arguments.
        */
        void log (SeverityLevels slvl, const char* format, std::va_list va);

    };
}

#endif //__LOGSDL_HPP__
