#ifndef __ICONTROLLER_HPP__
#define __ICONTROLLER_HPP__

/**
 * \file icontroller.hpp
 * \brief Interface for game controllers handling.
 * \author willl
 * \version 1.0
 */

// forward declaration
union SDL_Event;
namespace AnotherWorld {
    class SDLStub;

    /// \brief Namespace for handling game controllers.
    namespace Control {
        /// \brief Kind of controller being handled.
        enum Type {
            NONE,
            KEYBOARD,
            JOYSTICK,
            GAMEPAD
        };

        /**
         * \brief Controller describer.
         *
         * Pure virtual class for interfacing specialised describers.
         */
        class IController {
            public:
                /// \brief Destructor.
                virtual ~IController () {}

                /**
                 * \brief Open controller
                 * \param device Device identifier.
                 */
                virtual void open (int device) = 0;

                /// \brief Close controller.
                virtual void close () = 0;

                /**
                 * \brief Process controller events.
                 * \param event Event to be process.
                 * \param sys System describer.
                 * \returns Whether or not the processing is a success.
                 */
                virtual bool processEvents (const SDL_Event &event,
                                            SDLStub &sys) = 0;
        };
    }
}

#endif // __ICONTROLLER_HPP__
