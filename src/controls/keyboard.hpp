#ifndef __KEYBOARD_HPP__
#define __KEYBOARD_HPP__

/**
 * \file keyboard.hpp
 * \brief Definitions for handling joysticks and gamepads.
 * \author willll
 * \version 2.0
 */

#include "icontroller.hpp"

namespace AnotherWorld {
    namespace Control {
        class Keyboard : public AnotherWorld::Control::IController {
            public :
                /// \brief destructor
                ~Keyboard () {}

                /**
                 * \brief Open controller.
                 * \param device Controller identifier.
                 */
                void open (int device) override {(void)device;}

                /// Close controller
                void close () override  {}

                /**
                 * \brief Process controller events.
                 * \param event Event to be processed.
                 * \returns Whether or not it has been able to handle the event.
                 */
                bool processEvents(const SDL_Event &event,
                                   SDLStub& sys) override;
        };
    }
}

#endif // __KEYBOARD_HPP__
