/**
 * \file resources.cpp
 * \brief Read game assets.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willll
 * \author Glaize, Sylvain
 * \version 2.1
 */

#include "resources.hpp"

#include <iostream>

#include "bankReader.hpp"
#include "log.hpp"
#include "memListReader.hpp"
#include "parts.hpp"
#include "titleDetector.hpp"
#include "video.hpp"

const char* AnotherWorld::resTypeToString (unsigned int type) {
    /// Type to string table of equivalence.
    static const char *resTypes[] = {"RT_SOUND",     "RT_MUSIC",
                                     "RT_POLY_ANIM", "RT_PALETTE",
                                     "RT_BYTECODE",  "RT_POLY_CINEMATIC"};
    if (type >= (sizeof(resTypes) / sizeof(const char *))) {
        return "RT_UNKNOWN";
    }
    return resTypes[type];
}

void AnotherWorld::Resource::readBank (const MemEntry* me,
                                       std::uint8_t* dstBuf) const {
    /// Position of the entry.
    const std::size_t entryNumber = me - memList.data();
    Logger::getLogger().debug(
        ILogger::DBG_BANK, "Resource::readBank(%d)", entryNumber);

    BankReader bankReader(dataDir, me, dstBuf);
    if (!bankReader.isValid()) {
        Logger::getLogger().error(
            "Resource::readBank() unable to read file for bank entry.\n");
    }
}

void AnotherWorld::Resource::readEntries (const TitleDetection &detection) {
    switch (detection.getDataType()) {
        case DT_DOS:
            resourceCount =
                readEntriesForDos(dataDir, memList.data());
            break;
        case DT_ATARI:
            resourceCount =
                readEntriesForAtari(dataDir, memList.data());
            break;
        default:
            Logger::getLogger().error(
                "Resource::readEntries() unknown data. Stop.");
            std::cerr << "Cannot locate game assets, leaving.\n";
            exit(-7);
    }
}

void AnotherWorld::Resource::loadMarkedResourcesAsNeeded () {
    for (MemEntry* me; (me = getNextEntryToLoad()) != nullptr;) {
        if (me->bankId == 0) {
            Logger::getLogger().warning(
                "Resource::load() ec=0x%X (me->bankId == 0)", 0xF00);
            me->state = MemState::STATE_NOT_NEEDED;
        } else {
            loadResource(me);
        }
    }
}

void AnotherWorld::Resource::loadResource (MemEntry* me) {
    if (me->type == RT_POLY_ANIM) {
        std::uint8_t* loadDestination =
            memoryAllocator.getCurrentVideoPointer();

        logEntryLoad(me, loadDestination);
        readBank(me, loadDestination);

        video->copyPage(memoryAllocator.getCurrentVideoPointer());
        me->state = MemState::STATE_NOT_NEEDED;
    } else {
        if (me->size > memoryAllocator.getFreeScriptMemory()) {
            Logger::getLogger().warning("Resource::load() not enough memory");
            me->state = MemState::STATE_NOT_NEEDED;
            return;
        }

        /// Where data should be loaded.
        std::uint8_t *loadDestination =
            memoryAllocator.getCurrentScriptPointer();

        logEntryLoad(me, loadDestination);
        readBank(me, loadDestination);

        memoryAllocator.reserveMemoryForScript(me->size);
        me->bufPtr = loadDestination;
        me->state = MemState::STATE_LOADED;
    }
}

AnotherWorld::MemEntry* AnotherWorld::Resource::getNextEntryToLoad () {
    /// Entry index.
    std::uint16_t index = 0;
    /// Entry maximal rank.
    std::uint8_t maxRank = 0;

    /* Get resource with max rankNum
     * TBD : can it be replaced with a simple search? */
    /// Whether or not the resource has been found.
    bool resourceFound = false;
    for (auto it = 0; it < resourceCount; ++it) {
        /// Element to be tested.
        auto &elt = memList[it];
        if (elt.state == MemState::STATE_LOAD_ME && maxRank <= elt.rankNum) {
            maxRank = elt.rankNum;
            index = it;
            resourceFound = true;
        }
    }

    return resourceFound? &memList[index]: nullptr;
}

void AnotherWorld::Resource::invalidateRes () {
    for (auto it = 0; it < resourceCount; ++it) {
        /// Element to be treated.
        auto &elt = memList[it];
        if (elt.type <= RT_POLY_ANIM ||
            elt.type > 6) {  // 6 WTF ?!?! ResType goes up to 5 !!
            elt.state = MemState::STATE_NOT_NEEDED;
            if (elt.type > 6) {
                Logger::getLogger().warning(
                    "Resource::invalidateRes : %s, line %d : type out of range",
                    __FILE__, __LINE__);
            }
        }
    }
    memoryAllocator.freeResourceMemory();
}

void AnotherWorld::Resource::invalidateAll() {
    for (auto it = 0; it < resourceCount; ++it) {
        memList[it].state = MemState::STATE_NOT_NEEDED;
    }

    memoryAllocator.freeAllScriptMemory();
}

void AnotherWorld::Resource::loadPartsOrMemoryEntry (std::uint16_t resourceId) {
    if (resourceId > resourceCount) {
        requestedNextPart =
            static_cast<Game::PartIdentifier>(resourceId);
    } else {
        /// Entry to be treated.
        MemEntry &me = memList[resourceId];

        if (me.state == MemState::STATE_NOT_NEEDED) {
            me.state = MemState::STATE_LOAD_ME;
            loadMarkedResourcesAsNeeded();
        }
    }
}

void AnotherWorld::Resource::setupPart (Game::PartIdentifier partId) {
    if (partId == currentPartId_) {return;}

    if (partId < Game::PART_FIRST || partId > Game::PART_LAST) {
        Logger::getLogger().error(
            "Resource::setupPart() ec=0x%X invalid partId", partId);
    }

    /// Current part indices.
    const auto gamePartIndices = Parts::getResourceIndices(partId);
    /// Current palette.
    const auto paletteIndex = gamePartIndices.paletteIndex;
    /// Current part code.
    const auto codeIndex = gamePartIndices.codeIndex;
    /// Cut scene index.
    const auto videoCinematicIndex = gamePartIndices.videoCinematicIndex;
    /// To be completed ...
    const auto video2Index = gamePartIndices.video2Index;

    // Mark all resources as located on hard drive.
    invalidateAll();

    memList[paletteIndex].state = MemState::STATE_LOAD_ME;
    memList[codeIndex].state = MemState::STATE_LOAD_ME;
    memList[videoCinematicIndex].state = MemState::STATE_LOAD_ME;

    // This is probably a cinematic or a non interactive part of the game.
    // Player and enemy polygons are not needed.
    if (video2Index != Parts::UNREFERENCED) {
        memList[video2Index].state = MemState::STATE_LOAD_ME;
    }

    loadMarkedResourcesAsNeeded();

    segPalettes_ = memList[paletteIndex].bufPtr;
    segBytecode_ = memList[codeIndex].bufPtr;
    segCinematic_ = memList[videoCinematicIndex].bufPtr;

    // This is probably a cinematic or a non interactive part of the game.
    // Player and enemy polygons are not needed.
    if (video2Index != Parts::UNREFERENCED) {
        segVideo2 = memList[video2Index].bufPtr;
    }

    currentPartId_ = partId;

    // Protect the current allocations from freeResourceMemory()
    memoryAllocator.markScriptMemory();

    logPartSetup(partId, gamePartIndices);
}

std::uint8_t* AnotherWorld::Resource::getVideoBuffer () const {
    if (videoSegmentKind == USE_VIDEO_SEGMENT) {
        return segVideo2;
    } else {
        return segCinematic_;
    }
}

void AnotherWorld::Resource::logPartSetup (
        Game::PartIdentifier partId,
        const Parts::PartResourceIndices &gamePartIndices) {
    /// Current palette index.
    const auto paletteIndex = gamePartIndices.paletteIndex;
    /// Current game part code.
    const auto codeIndex = gamePartIndices.codeIndex;
    /// Current cut scene index.
    const auto videoCinematicIndex = gamePartIndices.videoCinematicIndex;
    /// To be completed ...
    const auto video2Index = gamePartIndices.video2Index;

    /// Structure for logs.
    auto &logger = Logger::getLogger();

    logger.debug(ILogger::DBG_RES, "");
    logger.debug(ILogger::DBG_RES, "setupPart(%d)", partId - Game::PART_FIRST);
    logger.debug(ILogger::DBG_RES, "Loaded resource %d (%s) in segPalettes.",
                 paletteIndex, resTypeToString(memList[paletteIndex].type));
    logger.debug(ILogger::DBG_RES, "Loaded resource %d (%s) in segBytecode.",
                 codeIndex, resTypeToString(memList[codeIndex].type));
    logger.debug(ILogger::DBG_RES, "Loaded resource %d (%s) in segCinematic.",
                 videoCinematicIndex,
                 resTypeToString(memList[videoCinematicIndex].type));

    if (video2Index != Parts::UNREFERENCED) {
        logger.debug(ILogger::DBG_RES, "Loaded resource %d (%s) in segVideo2.",
                     video2Index, resTypeToString(memList[video2Index].type));
    }
}

void AnotherWorld::Resource::logEntryLoad (
        const MemEntry* me, std::uint8_t* loadDestination) const {
    Logger::getLogger().debug(
        ILogger::DBG_BANK,
        "Resource::load() bufPos=%X size=%X type=%X pos=%X bankId=%X",
        memoryAllocator.getOffsetInMemory(loadDestination), me->packedSize,
        me->type, me->bankOffset, me->bankId);
}
