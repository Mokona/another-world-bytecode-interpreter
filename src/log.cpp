/**
 * \file log.cpp
 * \brief Class to manage logs
 * \author Willll
 * \version 0.1
 */

#include "log.hpp"

#ifdef BOOST_LOG_ENABLE
    #include "loggers/logBoost.hpp"
    using LoggingClass=AnotherWorld::LogBoost;
#else
    #include "loggers/logSDL.hpp"
    using LoggingClass=AnotherWorld::LogSDL;
#endif

#include <stdexcept>

namespace AnotherWorld {
    /// \brief Logger static object
    ILogger *Logger::logger = nullptr;

    ILogger &Logger::getLogger() {
        if (logger) {
            return *logger;
        } else {
            throw std::runtime_error("Logger not initialized");
        }
    }

    void Logger::loggerFactory (const LoggerConfiguration &configuration) {
        if (!logger) {
            Logger::logger = new LoggingClass(configuration.isLogEnable());
            Logger::logger->setup(configuration);
        } else {
            throw std::runtime_error("Logger already initialized");
        }
    }
}
