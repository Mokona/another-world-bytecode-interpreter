/**
 * \file video.cpp
 * \brief Draw game graphics.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.1
 */

#include "video.hpp"

#include <cassert>
#include <algorithm>
#include <utility>
#include <cstring>
#include <cstddef>
#include <algorithm>

#include "resources.hpp"
#include "sys.hpp"
#include "log.hpp"

AnotherWorld::Video::Video (Resource* _res, SDLStub* stub):
        res (_res), sys (stub), paletteIdRequested (0), currentPaletteId (0),
        frameBuffer (nullptr), pages (), curPagePtr1 (nullptr),
        curPagePtr2 (nullptr), curPagePtr3 (nullptr), polygon (), hliney (0),
        interpTable (), pData (), dataBuf (nullptr) {
    paletteIdRequested = NO_PALETTE_CHANGE_REQUESTED;

    frameBuffer = new std::uint8_t [nPages * VID_PAGE_SIZE];
    std::memset(frameBuffer, 0, nPages * VID_PAGE_SIZE);

    for (std::size_t i = 0; i < nPages; ++i) {
        pages[i] = frameBuffer + i * VID_PAGE_SIZE;
    }

    curPagePtr3 = getPage(1);
    curPagePtr2 = getPage(2);

    changePagePtr1(0xFE);

    interpTable[0] = 0x4000;

    for (std::uint16_t i = 1; i < interpTableSize; ++i) {
        interpTable[i] = 0x4000 / i;
    }
}

AnotherWorld::Video::Video (const Video &other):
        res (other.res), sys (other.sys),
        paletteIdRequested (other.paletteIdRequested),
        currentPaletteId (other.currentPaletteId),
        frameBuffer (other.frameBuffer), curPagePtr1 (other.curPagePtr1),
        curPagePtr2 (other.curPagePtr2), curPagePtr3 (other.curPagePtr3),
        polygon (other.polygon), hliney (other.hliney), pData (other.pData),
        dataBuf (other.dataBuf) {
    std::copy(other.interpTable, other.interpTable + interpTableSize,
              interpTable);
    std::copy(other.pages, other.pages + nPages, pages);
}

AnotherWorld::Video::~Video() {
    delete [] frameBuffer;
    frameBuffer = nullptr;
    for (std::size_t i = 0; i < nPages; ++i) {
        pages[i] = nullptr;
    }
}

AnotherWorld::Video &AnotherWorld::Video::operator = (const Video &other) {
    if (&other != this) {
        if (frameBuffer != nullptr) {
            delete [] frameBuffer;
            frameBuffer = nullptr;
            for (std::size_t i = 0; i < nPages; ++i) {
                pages[i] = nullptr;
            }
        }

        std::copy(other.interpTable, other.interpTable + interpTableSize,
                  interpTable);
        res = other.res;
        sys = other.sys;
        paletteIdRequested = other.paletteIdRequested;
        currentPaletteId = other.currentPaletteId;
        frameBuffer = other.frameBuffer;
        std::copy(other.pages, other.pages + nPages, pages);
        curPagePtr1 = other.curPagePtr1;
        curPagePtr2 = other.curPagePtr2;
        curPagePtr3 = other.curPagePtr3;
        polygon = other.polygon;
        hliney = other.hliney;
        pData = other.pData;
        dataBuf = other.dataBuf;
    }

    return *this;
}

AnotherWorld::Video &AnotherWorld::Video::operator = (Video &&other) {
    if (&other != this) {
        if (frameBuffer != nullptr) {
            delete [] frameBuffer;
            frameBuffer = nullptr;
            for (std::size_t i = 0; i < nPages; ++i) {
                pages[i] = nullptr;
            }
        }

        std::copy(other.interpTable, other.interpTable + interpTableSize,
                  interpTable);
        res = other.res;
        sys = other.sys;
        paletteIdRequested = other.paletteIdRequested;
        currentPaletteId = other.currentPaletteId;
        frameBuffer = other.frameBuffer;
        std::copy(other.pages, other.pages + nPages, pages);
        curPagePtr1 = other.curPagePtr1;
        curPagePtr2 = other.curPagePtr2;
        curPagePtr3 = other.curPagePtr3;
        polygon = other.polygon;
        hliney = other.hliney;
        pData = other.pData;
        dataBuf = other.dataBuf;

        other.res = nullptr;
        other.sys = nullptr;
        other.frameBuffer = nullptr;
        for (std::size_t i = 0; i < nPages; ++i) {
            other.pages[i] = nullptr;
        }
        other.curPagePtr1 = nullptr;
        other.curPagePtr2 = nullptr;
        other.curPagePtr3 = nullptr;
        other.dataBuf = nullptr;
    }

    return *this;
}

void AnotherWorld::Polygon::readVertices (const std::uint8_t* p,
                                          std::uint16_t zoom) {
    /// To be completed ...
    bbw = (*p++) * zoom / 64;
    /// To be completed ...
    bbh = (*p++) * zoom / 64;
    numPoints = *p++;
    assert((numPoints & 1) == 0 && numPoints < MAX_POINTS);

    /* Read all points, directly from bytecode segment. */
    for (std::uint8_t i = 0; i < numPoints; ++i) {
        Point *pt = &points[i];
        pt->x = (*p++) * zoom / 64;
        pt->y = (*p++) * zoom / 64;
    }
}

void AnotherWorld::Video::readAndDrawPolygon (std::uint8_t color,
                                              std::uint16_t zoom,
                                              const Point &pt) {
    /// Colour identifier.
    std::uint8_t i = pData.fetchByte();

    if (i >= 0xC0) {  // 0xc0 = 192
        if (color & 0x80) {    // 0x80 = 128 (1000 0000)
            color = i & 0x3F;  // 0x3F =  63 (0011 1111)
        }

        /* pc is misleading here since we are not reading bytecode but only
         * vertices informations. */
        polygon.readVertices(pData.pc, zoom);

        fillPolygon(color, zoom, pt);
    } else {
        i &= 0x3F;  // 0x3F = 63
        if (i == 1) {
            Logger::getLogger().warning("Video::readAndDrawPolygon() ec=0x%X (i != 2)",
                                        0xF80);
        } else if (i == 2) {
            readAndDrawPolygonHierarchy(zoom, pt);
        } else {
            Logger::getLogger().warning("Video::readAndDrawPolygon() ec=0x%X (i != 2)",
                                        0xFBB);
        }
    }
}

void AnotherWorld::Video::fillPolygon (std::uint8_t color, std::uint16_t zoom,
                                       const Point &pt) {
    if (polygon.bbw == 0 && polygon.bbh == 1 && polygon.numPoints == 4) {
        drawPoint(color, pt.x, pt.y);
        return;
    }

    std::int16_t x1 = pt.x - polygon.bbw / 2;
    std::int16_t x2 = pt.x + polygon.bbw / 2;
    const std::int16_t y1 = pt.y - polygon.bbh / 2;
    const std::int16_t y2 = pt.y + polygon.bbh / 2;

    if (x1 > 319 || x2 < 0 || y1 > 199 || y2 < 0) return;

    hliney = y1;

    std::uint16_t i, j;
    i = 0;
    j = polygon.numPoints - 1;

    x2 = polygon.points[i].x + x1;
    x1 = polygon.points[j].x + x1;

    ++i;
    --j;

    /// Drawing function identifier.
    drawLine drawFct;
    if (color < 0x10) {
        drawFct = &Video::drawLineN;
    } else if (color > 0x10) {
        drawFct = &Video::drawLineP;
    } else {
        drawFct = &Video::drawLineBlend;
    }

    /// To be completed ...
    std::uint32_t cpt1 = x1 << 16;
    /// To be completed ...
    std::uint32_t cpt2 = x2 << 16;

    while (1) {
        polygon.numPoints -= 2;
        if (polygon.numPoints == 0) {
            break;
        }
        /// To be completed ...
        std::uint16_t h;
        /// To be completed ...
        std::int32_t step1 = calcStep(polygon.points[j + 1],
                                      polygon.points[j], h);
        /// To be completed ...
        std::int32_t step2 = calcStep(polygon.points[i - 1],
                                      polygon.points[i], h);

        ++i;
        --j;

        cpt1 = (cpt1 & 0xFFFF0000) | 0x7FFF;
        cpt2 = (cpt2 & 0xFFFF0000) | 0x8000;

        if (h == 0) {
            cpt1 += step1;
            cpt2 += step2;
        } else {
            for (; h != 0; --h) {
                if (hliney >= 0) {
                    x1 = cpt1 >> 16;
                    x2 = cpt2 >> 16;
                    if (x1 <= 319 && x2 >= 0) {
                        if (x1 < 0) x1 = 0;
                        if (x2 > 319) x2 = 319;
                        (this->*drawFct)(x1, x2, color);
                    }
                }
                cpt1 += step1;
                cpt2 += step2;
                ++hliney;
                if (hliney > 199) return;
            }
        }
    }
}

void AnotherWorld::Video::readAndDrawPolygonHierarchy (std::uint16_t zoom,
                                                       const Point &pgc) {
    /// Hierarchy localisation.
    Point pt (pgc);
    pt.x -= pData.fetchByte() * zoom / 64;
    pt.y -= pData.fetchByte() * zoom / 64;

    /// Number of polygons in hierarchy.
    std::int16_t childs = pData.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "Video::readAndDrawPolygonHierarchy childs=%d",
                              childs);

    for (; childs >= 0; --childs) {
        /// Data offset in memory.
        std::uint16_t off = pData.fetchWord(sys->fileEndian());

        /// Current polygon localisation.
        AnotherWorld::Point po (pt);
        po.x += pData.fetchByte() * zoom / 64;
        po.y += pData.fetchByte() * zoom / 64;

        /// Current polygon colour.
        std::uint8_t color = 0xFF;
        /// To be completed ...
        uint16_t _bp = off;
        off &= 0x7FFF;

        if (_bp & 0x8000) {
            color = *pData.pc & 0x7F;
            pData.pc += 2;
        }

        /// To be completed ...
        std::uint8_t *bak = pData.pc;
        pData.pc = dataBuf + off * 2;

        readAndDrawPolygon(color, zoom, po);

        pData.pc = bak;
    }
}

std::int32_t AnotherWorld::Video::calcStep(const Point &p1, const Point &p2,
                                           std::uint16_t &dy) {
    dy = p2.y - p1.y;
    return (p2.x - p1.x) * interpTable[dy] * 4;
}

void AnotherWorld::Video::drawChar (std::uint8_t character, std::uint16_t x,
                                    std::uint16_t y, std::uint8_t color,
                                    std::uint8_t* buf) {
    if (x <= 39 && y <= 192) {
        /// Character fonts.
        const std::uint8_t* ft = _font + (character - ' ') * 8;

        /// Pointer to memory position.
        std::uint8_t* p = buf + x * 4 + y * 160;

        for (std::size_t j = 0; j < 8; ++j) {
            /// Actual character in the font.
            std::uint8_t ch = *(ft + j);
            for (std::size_t i = 0; i < 4; ++i) {
                /// To be completed ...
                std::uint8_t b = *(p + i);
                /// To be completed ...
                std::uint8_t cmask = 0xFF;
                /// To be completed ...
                std::uint8_t colb = 0;
                if (ch & 0x80) {
                    colb |= color << 4;
                    cmask &= 0x0F;
                }
                ch <<= 1;
                if (ch & 0x80) {
                    colb |= color;
                    cmask &= 0xF0;
                }
                ch <<= 1;
                *(p + i) = (b & cmask) | colb;
            }
            p += 160;
        }
    }
}

void AnotherWorld::Video::drawString (std::uint8_t color, std::uint16_t x,
                                      std::uint16_t y,
                                      std::uint16_t stringId) {
    /// The string to be displayed.
    const StrEntry* se = _stringsTableEng;

    /* Search for the location where the string is located. */
    while (se->id != END_OF_STRING_DICTIONARY && se->id != stringId) ++se;

    Logger::getLogger().debug(ILogger::DBG_VIDEO, "drawString(%d, %d, %d, '%s')",
                              color, x, y, se->str);

    /* Not found. */
    if (se->id == END_OF_STRING_DICTIONARY) return;

    /**
     * \brief String original abscissa.
     *
     * Used if the string contains a return carriage.
    */
    const std::uint16_t xOrigin = x;
    /// String length.
    std::size_t len = strlen(se->str);
    for (std::size_t i = 0; i < len; ++i) {
        if (se->str[i] == '\n') {
            y += 8;
            x = xOrigin;
            continue;
        }

        drawChar(se->str[i], x, y, color, curPagePtr1);
        ++x;
    }
}

void AnotherWorld::Video::drawPoint (std::uint8_t color, std::int16_t x,
                                     std::int16_t y) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "drawPoint(%d, %d, %d)",
                              color, x, y);
    if (x >= 0 && x <= 319 && y >= 0 && y <= 199) {
        /// Dot offset in memory.
        std::uint16_t off = y * 160 + x / 2;

        /// To be completed ...
        std::uint8_t cmasko;
        /// To be completed ...
        std::uint8_t cmaskn;
        if (x & 1) {
            cmaskn = 0x0F;
            cmasko = 0xF0;
        } else {
            cmaskn = 0xF0;
            cmasko = 0x0F;
        }

        /// To be completed ...
        std::uint8_t colb = (color << 4) | color;
        if (color == 0x10) {
            cmaskn &= 0x88;
            cmasko = ~cmaskn;
            colb = 0x88;
        } else if (color == 0x11) {
            colb = *(pages[0] + off);
        }
        /// To be completed ...
        const std::uint8_t b = *(curPagePtr1 + off);
        *(curPagePtr1 + off) = (b & cmasko) | (colb & cmaskn);
    }
}

void AnotherWorld::Video::drawLineBlend (std::int16_t x1, std::int16_t x2,
                                         std::uint8_t color) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "drawLineBlend(%d, %d, %d)",
                              x1, x2, color);
    /// To be completed ...
    const std::int16_t xmax = std::max(x1, x2);
    /// To be completed ...
    const std::int16_t xmin = std::min(x1, x2);
    /// To be completed ...
    std::uint8_t* p = curPagePtr1 + hliney * 160 + xmin / 2;

    /// To be completed ...
    std::uint16_t w = xmax / 2 - xmin / 2 + 1;
    /// To be completed ...
    std::uint8_t cmaske = 0;
    /// To be completed ...
    std::uint8_t cmasks = 0;
    if (xmin & 1) {
        --w;
        cmasks = 0xF7;
    }
    if (!(xmax & 1)) {
        --w;
        cmaske = 0x7F;
    }

    if (cmasks != 0) {
        *p = (*p & cmasks) | 0x08;
        ++p;
    }
    while (w--) {
        *p = (*p & 0x77) | 0x88;
        ++p;
    }
    if (cmaske != 0) {
        *p = (*p & cmaske) | 0x80;
    }
}

void AnotherWorld::Video::drawLineN (std::int16_t x1, std::int16_t x2,
                                     std::uint8_t color) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "drawLineN(%d, %d, %d)", x1,
                              x2, color);
    /// To be completed ...
    const std::int16_t xmax = std::max(x1, x2);
    /// To be completed ...
    const std::int16_t xmin = std::min(x1, x2);
    /// To be completed ...
    std::uint8_t* p = curPagePtr1 + hliney * 160 + xmin / 2;

    /// To be completed ...
    std::uint16_t w = xmax / 2 - xmin / 2 + 1;
    /// To be completed ...
    std::uint8_t cmaske = 0;
    /// To be completed ...
    std::uint8_t cmasks = 0;
    if (xmin & 1) {
        --w;
        cmasks = 0xF0;
    }
    if (!(xmax & 1)) {
        --w;
        cmaske = 0x0F;
    }

    /// Background colour.
    const std::uint8_t colb = ((color & 0xF) << 4) | (color & 0xF);
    if (cmasks != 0) {
        *p = (*p & cmasks) | (colb & 0x0F);
        ++p;
    }
    while (w--) {
        *p++ = colb;
    }
    if (cmaske != 0) {
        *p = (*p & cmaske) | (colb & 0xF0);
    }
}

void AnotherWorld::Video::drawLineP (std::int16_t x1, std::int16_t x2,
                                     std::uint8_t color) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "drawLineP(%d, %d, %d)", x1,
                              x2, color);
    /// To be completed.
    const std::int16_t xmax = std::max(x1, x2);
    /// To be completed.
    const std::int16_t xmin = std::min(x1, x2);
    /// To be completed.
    const std::uint16_t off = hliney * 160 + xmin / 2;
    /// To be completed.
    std::uint8_t* p = curPagePtr1 + off;
    /// To be completed.
    std::uint8_t* q = pages[0] + off;

    /// To be completed.
    std::uint8_t w = xmax / 2 - xmin / 2 + 1;
    /// To be completed.
    std::uint8_t cmaske = 0;
    /// To be completed.
    std::uint8_t cmasks = 0;
    if (xmin & 1) {
        --w;
        cmasks = 0xF0;
    }
    if (!(xmax & 1)) {
        --w;
        cmaske = 0x0F;
    }

    if (cmasks != 0) {
        *p = (*p & cmasks) | (*q & 0x0F);
        ++p;
        ++q;
    }
    while (w--) {
        *p++ = *q++;
    }
    if (cmaske != 0) {
        *p = (*p & cmaske) | (*q & 0xF0);
    }
}

std::uint8_t* AnotherWorld::Video::getPage (std::uint8_t page) {
    /// Pointer to the asked video buffer.
    std::uint8_t* p;
    if (page <= 3) {
        p = pages[page];
    } else {
        switch (page) {
            case 0xFF:
                p = curPagePtr3;
                break;
            case 0xFE:
                p = curPagePtr2;
                break;
            default:
                p = pages[0];  // XXX check
                Logger::getLogger().warning("Video::getPage() p != [0,1,2,3,0xFF,0xFE] == 0x%X",
                                            page);
                break;
        }
    }
    return p;
}

void AnotherWorld::Video::changePagePtr1 (std::uint8_t pageID) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "Video::changePagePtr1(%d)",
                              pageID);
    curPagePtr1 = getPage(pageID);
}

void AnotherWorld::Video::fillPage (std::uint8_t pageId, std::uint8_t color) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "Video::fillPage(%d, %d)",
                              pageId, color);
    /// To be completed.
    std::uint8_t* p = getPage(pageId);

    /**
     * \brief Actual colour value in memory.
     *
     * Since a palette indice is coded on 4 bits, we need to duplicate the
     * clearing color to the upper part of the byte.
     */
    std::uint8_t c = (color << 4) | color;

    std::memset(p, c, VID_PAGE_SIZE);
}

void AnotherWorld::Video::copyPage (std::uint8_t srcPageId,
                                    std::uint8_t dstPageId,
                                    std::int16_t vscroll) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "Video::copyPage(%d, %d)",
                              srcPageId, dstPageId);

    if (srcPageId == dstPageId) return;

    /// Pointer to source buffer.
    std::uint8_t* p;
    /// Pointer to destination buffer.
    std::uint8_t* q;

    if (srcPageId >= 0xFE || !((srcPageId &= 0xBF) & 0x80)) {
        p = getPage(srcPageId);
        q = getPage(dstPageId);
        std::memcpy(q, p, VID_PAGE_SIZE);
    } else {
        p = getPage(srcPageId & 3);
        q = getPage(dstPageId);
        if (vscroll >= -199 && vscroll <= 199) {
            /// Screen height.
            std::uint16_t h = 200;
            if (vscroll < 0) {
                h += vscroll;
                p += -vscroll * 160;
            } else {
                h -= vscroll;
                q += vscroll * 160;
            }
            std::memcpy(q, p, h * 160);
        }
    }
}

void AnotherWorld::Video::copyPage (const std::uint8_t* src) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "Video::copyPage()");
    /// Pointer to the beginning of displayed frame buffer.
    std::uint8_t *dst = pages[0];
    /// Screen height.
    std::size_t h = 200;
    while (h--) {
        /// To be completed ...
        std::size_t w = 40;
        while (w--) {
            /// To be completed ...
            std::uint8_t p [] = {*(src + 8000 * 3), *(src + 8000 * 2),
                                 *(src + 8000 * 1), *(src + 8000 * 0)};
            for (std::size_t j = 0; j < 4; ++j) {
                std::uint8_t acc = 0;
                for (std::size_t i = 0; i < 8; ++i) {
                    acc <<= 1;
                    acc |= (p[i & 3] & 0x80) ? 1 : 0;
                    p[i & 3] <<= 1;
                }
                *dst++ = acc;
            }
            ++src;
        }
    }
}

void AnotherWorld::Video::changePal (std::uint8_t palNum) {
    if (palNum >= 32) return;

    /// Pointer ti the asked palette.
    std::uint8_t* p =
        res->segPalettes() +
        palNum * 32;  // colors are coded on 2bytes (565) for 16 colors = 32
    sys->setPalette(p);
    currentPaletteId = palNum;
}

void AnotherWorld::Video::updateDisplay (std::uint8_t pageId) {
    Logger::getLogger().debug(ILogger::DBG_VIDEO, "Video::updateDisplay(%d)",
                              pageId);
    if (pageId != 0xFE) {
        if (pageId == 0xFF) {
            std::swap(curPagePtr2, curPagePtr3);
        } else {
            curPagePtr2 = getPage(pageId);
        }
    }

    // Check if we need to change the palette
    if (paletteIdRequested != NO_PALETTE_CHANGE_REQUESTED) {
        changePal(paletteIdRequested);
        paletteIdRequested = NO_PALETTE_CHANGE_REQUESTED;
    }

    // Q: Why 160 ?
    // A: Because one byte gives two palette indices so
    //   we only need to move 320/2 per line.
    sys->updateDisplay(curPagePtr2);
}
