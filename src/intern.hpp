#ifndef __INTERN_HPP__
#define __INTERN_HPP__

/**
 * \file intern.hpp
 * \brief Useful definitions.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.0
 */

#include <cstdint>

#include "endian.hpp"

/// \brief Namespace for the game objects and functions.
namespace AnotherWorld {
    /// \brief Pointer to fetch byte or word from memory.
    struct Ptr {
        /// \brief A pointer to some memory space.
        std::uint8_t* pc;

        /// \brief Constructor.
        Ptr (): pc (nullptr) {}

        /**
         * \brief Fetches a byte in memory and jump to the next byte.
         * \returns The value of the pointed byte.
         */
        std::uint8_t fetchByte () {return *pc++;}

        /**
         * \brief Fetches a word in memory and jump to the next word.
         * \param dataEndianness To what endianness data does comply.
         * \returns The value of the pointed word.
         */
        std::uint16_t fetchWord (Endian dataEndianness) {
            /// Fetched value;
            const std::uint16_t i =
                file2Native(*reinterpret_cast<std::uint16_t*>(pc),
                            dataEndianness);
            pc += 2;
            return i;
        }
    };

    /// \brief A 2D point.
    struct Point {
        /// Abscissa.
        std::int16_t x;

        /// Ordinate.
        std::int16_t y;

        /// \brief Default constructor.
        Point (): x (0), y (0) {}

        /**
         * \brief Constructs a Point using to coordinates.
         * \param _x Abscissa.
         * \param _y Ordinate.
         */
        Point (std::int16_t _x, std::int16_t _y): x (_x), y (_y) {}
    };
}

#endif
