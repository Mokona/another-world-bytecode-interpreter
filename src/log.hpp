#ifndef __LOG_HPP__
#define __LOG_HPP__

/**
 * \file log.hpp
 * \brief Class to manage logs
 * \author Willll
 * \version 0.1
 */

#include <cstdarg>
#include <cstdint>

#include "ilog.hpp"

namespace AnotherWorld {
    /// \brief Class for execution logging.
    class Logger {
        public :

            /// \brief No default constructor.
            Logger() = delete;

            /**
             * \brief How to access the Logger, should be called to retrieve the
             * Logger object.
             * \returns The Logger object.
             */
            static ILogger &getLogger ();

            /**
             * \brief Logger factory method
             * \param configuration logger configuration object.
             */
            static void loggerFactory (const LoggerConfiguration &configuration);

        private:
            /**
            * \brief Syslog sink configuration method
            * \param configuration logger configuration object.
            */
            static void setupSyslog (const LoggerConfiguration &configuration);

            /// Badly designed static ILogger object.
            static ILogger *logger;
    };
}
#endif
