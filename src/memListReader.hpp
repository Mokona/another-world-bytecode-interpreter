#ifndef __RAW_SRC_MEMLISTREADER_HPP__
#define __RAW_SRC_MEMLISTREADER_HPP__

/**
 * \file memListReader.hpp
 * \brief Definitions to manage game assets.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willll
 * \author Glaize, Sylvain
 * \version 1.0
 */

#include <cstdint>
#include <string>

class MemEntry;

namespace AnotherWorld {
    /**
     * \brief Read game assets from MS-DOS versions.
     * \param directory Directory where data are located.
     * \param memList Memory location to store data.
     */
    std::int16_t readEntriesForDos (const std::string &directory,
                                    MemEntry* memList);

    /**
     * \brief Read game assets from Atari ST versions.
     * \param directory Directory where data are located.
     * \param memList Memory location to store data.
     */
    std::int16_t readEntriesForAtari (const std::string &directory,
                                      MemEntry* memList);
}

#endif
