/**
 * \file memListReader.cpp
 * \brief Reads the resource directory.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willll
 * \author Glaize, Sylvain
 * \version 1.0
 */

#include "memListReader.hpp"

#include <iostream>
#include <cstddef>

#include "file.hpp"
#include "log.hpp"
#include "parts.hpp"
#include "resourceStats.hpp"
#include "video.hpp"

namespace AnotherWorld {
    std::int16_t readEntriesForAtari (const std::string &directory,
                                      MemEntry* memList) {
        /// Describer of the file to be read.
        File f;

        if (!openFile(f, "START.PRG", directory, bigEndian)) {
            Logger::getLogger().error(
                "Resource::readEntries() unable to open 'START.PRG' file\n");
            std::cerr << "Error: cannot locate game data files.\n";
            std::exit(-6);
        }

        /// Number of Entries composing the file signature.
        const std::size_t SIGNATURE_LENGTH = 20;
        /// Actual file signature.
        std::array<uint8_t, SIGNATURE_LENGTH> signature = {
            0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1a, 0x3c,
            0x00, 0x00, 0x1a, 0x3c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        /// Signature localisation.
        std::size_t signatureIndex = 0;
        /// Index offset in memory.
        std::uint32_t offset = 0;

        /// Maximum index for data.
        std::size_t maximumIndex = 0;

        while (!f.eof()) {
            /// Current byte in file.
            const auto readByte = f.read<std::uint8_t>();
            if (!f.good()) {
                /// Error message.
                const char* message =
                    "An error occurred while reading \"START.PRG\"\n";
                Logger::getLogger().error(message);
                std::cerr << message;
                std::exit(-7);
            }
            offset += 1;
            if (readByte == signature[signatureIndex]) {
                maximumIndex = std::max(maximumIndex, signatureIndex);
                signatureIndex += 1;
                if (signatureIndex == SIGNATURE_LENGTH) {
                    break;
                }
            } else if (readByte == signature[0]) {
                signatureIndex = 1;
            } else {
                signatureIndex = 0;
            }
        }

        if (signatureIndex != SIGNATURE_LENGTH) {
            Logger::getLogger().error(
                "Resource::readEntries() memList signature could not be found "
                "in "
                "'START.PRG' file\n");
            std::cerr << "Error: cannot validate game executable file.\n";
            std::exit(-7);
        }

        offset -= SIGNATURE_LENGTH * 2;
        f.seek(offset);

        /// Statistics container.
        ResourceStats resourceStats;

        /// Data localisation in memory.
        auto memEntry = memList;
        /// Asset identifier.
        std::int16_t numMemList = 0;
        while (true) {
            // assert(numMemList < memList.max_size());
            /// Current entry being loaded.
            memEntry->state = MemState::STATE_NOT_NEEDED;
            memEntry->type = f.read<std::uint8_t>();
            memEntry->bufPtr = nullptr;
            memEntry->rankNum = 0;
            memEntry->bankId = f.read<std::uint8_t>();
            memEntry->bankOffset = f.read<std::uint32_t>();
            memEntry->packedSize = f.read<std::uint32_t>();
            memEntry->size = f.read<std::uint32_t>();
            memEntry->fileEndianness = f.endianness();

            // Skip padding
            f.read<std::uint32_t>();
            f.read<std::uint16_t>();

            if (!f.good()) {
                /// Error message.
                const char* message =
                    "An error occurred while reading \"memlist.bin\"\n";
                Logger::getLogger().error(message);
                std::cerr << message;
                std::exit(-7);
            }

            if (memEntry->size == std::numeric_limits<uint32_t>::max()) {
                break;
            }

            // Memory tracking
            resourceStats.addEntry(*memEntry);

            ++numMemList;
            ++memEntry;
        }

        resourceStats.logSummary();

        return numMemList;
    }

    std::int16_t readEntriesForDos(const std::string& directory,
                                   MemEntry* memList) {
        /// Describer of the file to be read.
        File f;

        if (!openFile(f, "memlist.bin", directory, bigEndian)) {
            Logger::getLogger().error(
                "Resource::readEntries() unable to open 'memlist.bin' file\n");
            std::cerr << "Error: cannot locate game data files.\n";
            std::exit(-7);
        }

        /// Statistics container.
        ResourceStats resourceStats;

        /// Data localisation in memory.
        auto memEntry = memList;
        /// Counter of assets in bank.
        std::int16_t resourceCount = 0;
        while (true) {
            // assert(resourceCount < memList.max_size());
            /// Current entry being loaded.
            memEntry->state = f.read<std::uint8_t>();
            memEntry->type = f.read<std::uint8_t>();
            memEntry->bufPtr = nullptr;
            f.read<std::uint32_t>();
            memEntry->rankNum = f.read<std::uint8_t>();
            memEntry->bankId = f.read<std::uint8_t>();
            memEntry->bankOffset = f.read<std::uint32_t>();
            memEntry->packedSize = f.read<std::uint32_t>();
            memEntry->size = f.read<std::uint32_t>();
            memEntry->fileEndianness = f.endianness();

            if (!f.good()) {
                /// Error message.
                const char* message =
                    "An error occurred while reading 'memlist.bin'\n";
                Logger::getLogger().error(message);
                std::cerr << message;
                std::exit(-7);
            }

            if (memEntry->state == MemState::STATE_END_OF_MEMLIST) {
                break;
            }

            // Memory tracking
            resourceStats.addEntry(*memEntry);

            ++resourceCount;
            ++memEntry;
        }

        resourceStats.logSummary();

        return resourceCount;
    }

}
